var homeVC = {};
    homeVC.selectedTags = [];
    homeVC.$tagsCheckboxes = null;
    homeVC.$notes = null;

    homeVC.initView = function() {

        homeVC.$tagsCheckboxes = $('input[type="checkbox"]');
        homeVC.$notes = $('[data-function="note"]');

        homeVC.$tagsCheckboxes.on('change', homeVC.onCheckboxClick);

        // init notes links
        $.each($('[data-function="note-link"]'), function(index, value) {
            var $element = $(value);
            var id = $element.parent().attr("note-id");
            $element.attr('href', window.location.origin + '/notes/' + id + '/details');
        });

        // bind buttons - filter, add note
        $('[data-function="select-all"]').click(function() {
            homeVC.onChangeAllClick(true);
        });
        $('[data-function="unselect-all"]').click(function() {
            homeVC.onChangeAllClick(false);
        });
        $('[data-function="save-note"]').click(function() {
            homeVC.onSaveNoteClick();
        });
    };

    homeVC.onChangeAllClick = function(isSelect) {

        // selec / unselect all tags
        homeVC.$tagsCheckboxes.each(function() {
            this.checked = isSelect;
        });
        homeVC.onCheckboxClick();
    };

    homeVC.onCheckboxClick = function() {
        // filter notes by selected tags
        homeVC.selectedTags = [];
        homeVC.$tagsCheckboxes.filter(':checked').each(function() {
            homeVC.selectedTags.push(this.value);
        });
        var $filteredNotes = $('[data-function="note"]');
        $filteredNotes.each(function() {
            var $note = $(this);
            $note.hide();
            var noteTags = $(this).attr('tags').split(' ');
            // note has no tags
            if (noteTags.length === 1 && noteTags[0] === '') {
                $note.show();
                return;
            }
            // check for tags match
            var isMatched = false;
            $.each(homeVC.selectedTags, function(index, tag) {
                if ($.inArray(tag, noteTags) !== -1) {
                    isMatched = true;
                    return;
                }
            });
            if (isMatched) {
                $note.show();
            }
        });
    };

    homeVC.onSaveNoteClick = function() {
        // add note, redirect to details / management page
        var parameters = {
            name:        $('.note-name').val(),
            description: $('.note-description').val(),
            color:       'yellow'
        };
        apiClient.send('POST', 'notes', parameters, function(response) {
            window.location = window.location.origin + '/notes/' + response.data.id + '/details';
        }, function(response) {
            console.log('ERROR');
            console.log(response);
        })
    };

$(document).ready(function(){
    homeVC.initView();
});
