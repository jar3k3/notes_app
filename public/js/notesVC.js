var notesVC = {};
notesVC.$itemsCheckboxes = null;
notesVC.noteId = null

notesVC.initView = function() {

    // get note id
    var url = window.location.href;
    notesVC.noteId = url.match("/notes/(.*)/details")[1];

    notesVC.$itemsCheckboxes = $('input[type="checkbox"]');
    // bind switch is done status of item
    notesVC.$itemsCheckboxes.on('change', function() {
        // change status
        var id =  $(this).parent().attr("item-id");
        var is_checked = 1 - $(this).attr('data-is-checked');
        $(this).attr('data-is-checked', is_checked);
        notesVC.onCheckboxClick(id, is_checked);
    });

    // bind add / delete item, tag, comment buttons
    $('[data-function="save-item"]').click(function() {
        notesVC.onSaveItemClick();
    });
    $('[data-function="save-tag"]').click(function() {
        notesVC.onSaveTagClick();
    });
    $('[data-function="save-comment"]').click(function() {
        notesVC.onSaveCommentClick();
    });
    $('[data-function="delete-item"]').click(function() {
        var id =  $(this).parent().attr("item-id");
        notesVC.onDeleteItemClick(id);
    });
    $('[data-function="delete-tag"]').click(function() {
        var id =  $(this).parent().attr("tag-id");
        notesVC.onDeleteTagClick(id);
    });
    $('[data-function="delete-comment"]').click(function() {
        var id =  $(this).parent().parent().attr("comment-id");
        console.log(id);
        notesVC.onDeleteCommentClick(id);
    });
};

notesVC.onCheckboxClick = function(id, is_checked) {

    var parameters = {
        is_checked: is_checked
    };
    apiClient.send('PUT', 'notes/' + notesVC.noteId + '/items/' + id, parameters, function(response) {

        // status has been changed

    }, function(response) {
        console.log('ERROR');
        console.log(response);
    })
}

notesVC.onSaveItemClick = function() {
    // add item
    var parameters = {
        note_id:    notesVC.noteId,
        content:    $('.item-text').val(),
        is_checked: 0
    };
    apiClient.send('POST', 'notes/' + notesVC.noteId + '/items', parameters, function(response) {

        // append item to list
        var $item = $(notesVC.itemHTMLTemplate);
            $item.attr('item-id', response.data.id);
            $item.attr('data-is-checked', response.data.is_checked);
            $item.find('.form-check-label').html(response.data.content);
            $item.find('.form-check-input').attr('data-is-checked', response.data.is_checked);
            $item.find('.form-check-input').attr('id', 'item-' + response.data.id);
            $item.find('.form-check-label').attr('for', 'item-' + response.data.id);

        $('[data-function="note-items"]').append($item);
        $('#modalAddItem').modal('hide');

        // bind switch is done status of item
        $item.on('change', function() {
            // change status
            var is_checked = 1 - $item.attr('data-is-checked');
            $item.attr('data-is-checked', is_checked);
            notesVC.onCheckboxClick(response.data.id, is_checked);
        });

        // bind delete button
        $item.find('[data-function="delete-item"]').click(function() {
            notesVC.onDeleteItemClick(response.data.id);
        });

    }, function(response) {
        console.log('ERROR');
        console.log(response);
    })
};

notesVC.onSaveTagClick = function() {
    // add tag
    var parameters = {
        note_id: notesVC.noteId,
        name:    $('.tag-name').val()
    };
    apiClient.send('POST', 'notes/' + notesVC.noteId + '/tags', parameters, function(response) {

        // append tag to list
        var $tag = $(notesVC.tagHTMLTemplate);
            $tag.attr('tag-id', response.data.id);
            $tag.find('.form-check-label').html(response.data.name);

        $('[data-function="note-tags"]').append($tag);
        $('#modalAddTag').modal('hide');

        // bind delete button
        $tag.find('[data-function="delete-tag"]').click(function() {
            notesVC.onDeleteTagClick(response.data.id);
        });

    }, function(response) {
        console.log('ERROR');
        console.log(response);
    })
};

notesVC.onSaveCommentClick = function() {
    // add comment
    var parameters = {
        note_id: notesVC.noteId,
        content: $('.comment-text').val()
    };
    apiClient.send('POST', 'notes/' + notesVC.noteId + '/comments', parameters, function(response) {

        // append comment to list
        var $comment = $(notesVC.commentHTMLTemplate);
            $comment.attr('comment-id', response.data.id);
            $comment.find('.form-check-label').html(response.data.name);
            $comment.find('[data-function="comment-content"]').html(response.data.content);
            $comment.find('[data-function="comment-created-by"]').html(
                'Created by ' + response.data.created_by + '. <br/>' +
                'Created at ' + response.data.created_at + '.'
            );

        $('[data-function="note-comments"]').append($comment);
        $('#modalAddComment').modal('hide');

        // bind delete button
        $comment.find('[data-function="delete-comment"]').click(function() {
            notesVC.onDeleteCommentClick(response.data.id);
        });

    }, function(response) {
        console.log('ERROR');
        console.log(response);
    })
};

notesVC.onDeleteItemClick = function(id) {
    // delete item
    apiClient.send('DELETE', 'notes/' + notesVC.noteId + '/items/' + id, {}, function(response) {

        // remove item from list
        $('[item-id="' + id + '"]').remove();

    }, function(response) {
        console.log('ERROR');
        console.log(response);
    });
};

notesVC.onDeleteTagClick = function(id) {
    // delete tag
    apiClient.send('DELETE', 'notes/' + notesVC.noteId + '/tags/' + id, {}, function(response) {

        // remove tag from list
        $('[tag-id="' + id + '"]').remove();

    }, function(response) {
        console.log('ERROR');
        console.log(response);
    });
};

notesVC.onDeleteCommentClick = function(id) {
    // delete tag
    apiClient.send('DELETE', 'notes/' + notesVC.noteId + '/comments/' + id, {}, function(response) {

        // remove tag from list
        $('[comment-id="' + id + '"]').remove();

    }, function(response) {
        console.log('ERROR');
        console.log(response);
    });
};

/* item,  tag, comment HTML templates */

notesVC.itemHTMLTemplate = `
    <label item-id="" class="form-check" data-function="item">
        <a class="close" data-function="delete-item" href="#">×</a>
        <input class="form-check-input" type="checkbox">
        <label class="form-check-label"></label>
    </label>
`;

notesVC.tagHTMLTemplate = `
    <label tag-id="" class="form-check data-function="tag"">
        <a class="close" data-function="delete-tag" href="#">×</a>
        <span class="form-check-label"></span>
    </label>
`;

notesVC.commentHTMLTemplate = `
    <div comment-id="" class="card mb-4 mr-3 w-25" data-function="comment">
        <div class="card-body">
            <a class="close" data-function="delete-comment" href="#">×</a>
            <div data-function="comment-content"></div>
            <hr>
            <div data-function="comment-created-by"></div>
        </div>
    </div>
`;

$(document).ready(function(){
    notesVC.initView();
});
