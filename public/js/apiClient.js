var apiClient = {};

    /**
     * Send request to api.
     */
    apiClient.send = function(method, url, parameters, onSuccessFunction, onErrorFunction) {
        if ($.inArray(method, ['GET', 'POST', 'PUT', 'DELETE']) === -1) {
            console.log('Invalid method, only get, post, put or delete are available');
            return;
        }

        var onSuccess = typeof onSuccessFunction === 'undefined' ? apiClient.onSuccessDefault : onSuccessFunction;
        var onError   = typeof onErrorFunction   === 'undefined' ? apiClient.onErrorDefault   : onErrorFunction;

        var ajaxParameters = {
            dataType:    'json',
            type:        method,
            url:         window.location.origin + '/api/' + url,
            data:        JSON.stringify(parameters),
            contentType: 'application/json',
            success:     onSuccess,
            error:       onError
        }
        console.log(ajaxParameters);

        $.ajax(ajaxParameters);
    };

    // default on success function
    apiClient.onSuccessDefault = function() {
        console.log('SUCCESS');
    };

    // default on error function
    apiClient.onErrorDefault = function() {
        console.log('ERROR');
    };
