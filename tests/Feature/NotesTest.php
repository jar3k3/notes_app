<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;

class NotesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testNotes()
    {
        $noteData = [
            'name'        => 'test',
            'description' => 'description',
            'color'       => 'color'
        ];

        DB::beginTransaction();

        // add note
        $response = $this->post('/api/notes', $noteData);
        $response->assertStatus(201);
        $noteData = json_decode($response->getContent());
        $noteId = $noteData->data->id;

        var_dump($noteId);

        // edit note
        $response = $this->put('/api/notes/' . $noteId, ['name' => 'edited']);
        $response->assertStatus(200);
        $noteItemData = json_decode($response->getContent());
        $this->assertEquals('edited', $noteItemData->data->name);

        // add tag
        $response = $this->post('/api/notes/' . $noteId . '/tags', ['name' => 'name']);
        $response->assertStatus(201);

        // delete note
        $response = $this->delete('/api/notes/' . $noteId, []);
        $response->assertStatus(204);

        // check if deleted
        $response = $this->get('/api/notes/' . $noteId);
        $response->assertStatus(404);

        DB::rollback();
    }
}
