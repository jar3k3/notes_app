@extends('layouts.app')

@section('scripts')
<script src="{{ asset('/js/homeVC.js') }}"></script>
@endsection

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="notes-container">
                @foreach ($notes as $note)
                    <div note-id="{{ $note->id }}" class="card mb-4" data-function="note" tags="{{ $note->toArray(null)['tags_string'] /* custom resource attribute */ }}">
                        <!--<h6 class="card-header" data-function="note-name"></h6>-->
                        <a class="card-header" data-function="note-link" href="#">{{ $note->name }}</a>
                        <div class="card-body" data-function="note-description">
                            {{ $note->description }}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-3">
            <div class="row">
                <button type="button" class="btn btn-primary btn-sm mb-4 w-100" data-function="add-note" data-toggle="modal" data-target="#modalAddNote">Add note</button>
            </div>
            <div class="row">
                <div class="card w-100">
                    <h5 class="card-header">Tags filter</h5>
                    <div class="card-body">
                        <div class="filter-container">
                            <div class="filter-functions-container">
                                <button type="button" class="btn btn-primary btn-sm mr-3" data-function="select-all">Select all</button>
                                <button type="button" class="btn btn-primary btn-sm" data-function="unselect-all">Unselect all</button>
                            </div>
                            <hr>
                            <div class="filter-tags-container">
                                <form>
                                    @foreach ($tagsNames as $tagName)
                                        <label class="form-check">
                                            <input class="form-check-input" type="checkbox" value="{{ $tagName }}" checked>
                                            <span class="form-check-label">
                                                {{ $tagName }}
                                            </span>
                                        </label>
                                    @endforeach
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- add note modal -->
<div class="modal fade" id="modalAddNote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header text-center">
            <h4 class="modal-title w-100 font-weight-bold">Add note</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body mx-3">
            <div class="md-form mb-5">
                <i class="fa fa-user prefix grey-text"></i>
                <label data-error="wrong" data-success="right" for="note-name">Name</label>
                <textarea class="note-name w-100 noresize" rows="1" placeholder="" required></textarea>
                <label data-error="wrong" data-success="right" for="note-description">Description</label>
                <textarea class="note-description w-100" rows="5" placeholder=""></textarea>
            </div>
        </div>
        <div class="modal-footer d-flex justify-content-center">
            <button class="btn btn-deep-orange" data-function="save-note">Add</button>
        </div>
        </div>
    </div>
</div>
@endsection
