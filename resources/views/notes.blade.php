@extends('layouts.app')

@section('styles')
    <link href="{{ asset('css/notes.css') }}" rel="stylesheet">
@endsection

@section('scripts')
    <script src="{{ asset('/js/notesVC.js') }}"></script>
@endsection

@section('content')

    <div class="container">
        <div class="row"> <!-- note data -->
            <div class="card mb-4 w-100" data-function="note">
                <h6 class="card-header" data-function="note-name">{{ $data['name'] }}</h6>
                <div class="card-body">
                    <div data-function="note-description">
                        <span><b>Description:</b></span><br/>
                        {{ $data['description'] }}
                    </div>
                    <div class="mt-4">
                        <form data-function="note-items">
                            <span><b>Items list:</b></span><br/>
                            @foreach ($data->items as $item)
                                <label class="form-check" item-id="{{ $item->id }}">
                                    <a class="close" data-function="delete-item" href="#">×</a>
                                    <input id="item-{{ $item->id }}" class="form-check-input" type="checkbox" {{ $item->is_checked == 1 ? 'checked' : ''}} data-is-checked="{{ $item->is_checked }}">
                                    <label class="form-check-label" for="item-{{ $item->id }}">{{ $item->content }}</label>
                                </label>
                            @endforeach
                        </form>
                    </div>
                    <div data-function="note-tags">
                        <span><b>Tags:</b></span><br/>
                        @foreach ($data->tags as $tag)
                            <label class="form-check" tag-id="{{ $tag->id }}">
                                <a class="close" data-function="delete-tag" href="#">×</a>
                                <span class="form-check-label">{{ $tag->name }}</span>
                            </label>
                        @endforeach
                    </div>
                    <hr>
                    <div data-function="note-created-by">
                        Created by {{ $data->toArray(null)['created_by'] /* custom resource attribute */ }}. <br/>
                        Created at {{ $data['created_at']}}.
                    </div>
                </div>
            </div>
        </div>
        <div class="row row justify-content-center">
            <button type="button" class="btn btn-primary btn-sm mb-4 mr-3 w-25" data-function="add-item" data-toggle="modal" data-target="#modalAddItem">Add item</button>
            <button type="button" class="btn btn-primary btn-sm mb-4 mr-3 w-25" data-function="add-tag" data-toggle="modal" data-target="#modalAddTag">Add tag</button>
            <button type="button" class="btn btn-primary btn-sm mb-4 mr-3 w-25" data-function="add-comment" data-toggle="modal" data-target="#modalAddComment">Add comment</button>
        </div>
        <div class="row row justify-content-center" data-function="note-comments"> <!-- comments -->
            @foreach ($data['comments'] as $comment)
                <div class="card mb-4 mr-3 w-25" data-function="comment" comment-id="{{ $comment->id }}">
                    <div class="card-body">
                        <a class="close" data-function="delete-comment" href="#">×</a>
                        <div data-function="comment-content">
                            {{ $comment['content'] }}
                        </div>
                        <hr>
                        <div data-function="comment-created-by">
                            Created by {{ $comment['created_by'] /* custom resource attribute */ }}. <br />
                            Created at {{ $comment['created_at']}}.
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <!-- add item modal -->
    <div class="modal fade" id="modalAddItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Add item</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">
                    <i class="fa fa-user prefix grey-text"></i>
                    <label data-error="wrong" data-success="right" for="item-text">Item text</label>
                    <textarea class="item-text w-100" rows="1" placeholder="" required></textarea>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-deep-orange" data-function="save-item">Add</button>
            </div>
            </div>
        </div>
    </div>

    <!-- add tag modal -->
    <div class="modal fade" id="modalAddTag" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Add tag</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">
                    <i class="fa fa-user prefix grey-text"></i>
                    <label data-error="wrong" data-success="right" for="tag-name">Tag name</label>
                    <textarea class="tag-name w-100" rows="1" placeholder="" required></textarea>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-deep-orange" data-function="save-tag">Add</button>
            </div>
            </div>
        </div>
    </div>

    <!-- add comment modal -->
    <div class="modal fade" id="modalAddComment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Add comment</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">
                    <i class="fa fa-user prefix grey-text"></i>
                    <label data-error="wrong" data-success="right" for="comment-text">Comment text</label>
                    <textarea class="comment-text w-100" rows="5" placeholder="" required></textarea>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-deep-orange" data-function="save-comment">Add</button>
            </div>
            </div>
        </div>
    </div>

@endsection
