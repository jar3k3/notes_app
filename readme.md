### 1. Description  
Application for notes management, created using Laravel framework.

### 2. Env onfiguration
- app url
- database settings / credentials

### 3. Commands
To seed database with example data run:
```
php artisan db:seed
```
To perform tests run:
```
vendor\bin\phpunit
```

### 4. To do
- use js apidoc to generate api documentation
- use TinyMCE for notes comments
- use factories in seeders, tests
- mix js
- middleware (require login for api methods, using token instead of login for tests)
- extend tests
- extend validation and error handling
- extend notes management
- set order for notes, notes items
- set color for notes
