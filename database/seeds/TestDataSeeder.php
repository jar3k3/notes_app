<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use App\Models\Note;
use App\Models\NoteItem;
use App\Models\NoteComment;
use App\Models\NoteTag;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            $userData = [
                'username'  => 'user7',
                'email'     => 'test@test.test',
                'firstname' => 'John',
                'lastname'  => 'Shepard',
                'password'  => Hash::make('password')
            ];
            $notesData = [
                [
                    'name' => 'Shopping list',
                    'description' => 'List of things to buy before Tommy\'s party.'
                ], [
                    'name' => 'Application',
                    'description' => 'List of project tasks. Deadline: 22.12.2018'
                ], [
                    'name' => 'Studies',
                    'description' => 'List of exams in this semester.'
                ], [
                    'name' => 'Empty',
                    'description' => 'Note without tags.'
                ]
            ];
            $noteItemsData = [
                [
                    'content'    => 'Buy somethink to drink.',
                    'is_checked' => 1
                ], [

                    'content'    => 'Buy a gift.',
                    'is_checked' => 0
                ]
            ];
            $noteCommentsContentData = [
                'Hurry up! 5 hours left...',
                'No comment'
            ];
            $noteTagsNamesData = [
                'shopping',
                'important',
                'important'
            ];

            DB::beginTransaction();

            // create test user
            $user = User::create($userData);

            // create test notes
            $notes = [];
            foreach ($notesData as $noteData) {
                $noteData = array_merge($noteData, ['user_id' => $user->id, 'color' => 'yellow']);
                $notes[] = Note::create($noteData);
            }
            $noteId = $notes[0]->id;

            // create test items for first note
            foreach ($noteItemsData as $noteItemData) {
                NoteItem::create([
                    'note_id'      => $noteId,
                    'content'      => $noteItemData['content'],
                    'is_checked'   => $noteItemData['is_checked'],
                    'order_number' => 1
                ]);
            }

            // create test comments for first note
            foreach($noteCommentsContentData as $noteCommentContentData) {
                NoteComment::create([
                    'note_id' => $noteId,
                    'user_id' => $user->id,
                    'content' => $noteCommentContentData
                ]);
            }

            // create test tags for first note
            foreach($noteTagsNamesData as $index => $noteTagNameData) {
                NoteTag::create([
                    'note_id' => $notes[$index]->id,
                    'name'    => $noteTagNameData
                ]);
            }

            DB::commit();

        } catch(\Exception $ex) {
            DB::rollback();
            echo $ex->getMessage() . ', line: ' . $ex->getLine() . '\n';
        }
    }
}
