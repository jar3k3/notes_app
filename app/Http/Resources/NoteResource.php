<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\NoteTag;
use App\Http\Resources\UserResource;
use App\Http\Resources\NoteItemResource;
use App\Http\Resources\NoteTagResource;
use App\Http\Resources\NoteCommentResource;

class NoteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'description' => $this->descpition,
            'color'       => $this->color,
            'created_at'  => (string) $this->created_at,
            'created_by'  => $this->user->firstname . ' ' . $this->user->lastname,
            //'user'        => new UserResource($this->user),
            'items'       => NoteItemResource::collection($this->items),
            'tags'        => NoteTagResource::collection($this->tags),
            'comments'    => NoteCommentResource::collection($this->comments),
            'tags_string' => implode(array_column($this->tags->toArray(), 'name'), ' ')
        ];
    }
}
