<?php

namespace App\Http\Controllers;

use App\Models\Note;
use App\Models\NoteTag;
use App\Http\Resources\NoteTagResource;
use Illuminate\Http\Request;

class NotesTagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function index(Note $note)
    {
        return NoteTagResource::collection($note->items()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Note $note)
    {
        $noteTag = NoteTag::create([
            'note_id' => $note->id,
            'name'    => $request->name
        ]);
        return new NoteTagResource($noteTag);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NoteTag  $noteTag
     * @param  Integer  $noteTagId
     * @return \Illuminate\Http\Response
     */
    public function show(NoteTag $noteTag)
    {
        return new NoteTagResource(NoteTag::find($noteTagId));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Note  $note
     * @param  Integer  $noteTagId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Note $note, $noteTagId)
    {
        $noteTag = NoteTag::findOrFail($noteTagId);
        $noteTag->update($request->only([
            'name'
        ]));
        return new NoteTagResource($noteTag);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Note  $note
     * @param  Integer  $noteTagId
     * @return \Illuminate\Http\Response
     */
    public function destroy(Note $note, $noteTagId)
    {
        $noteTag = NoteTag::findOrFail($noteTagId);
        $noteTag->delete();
        return response()->json(null, 204);
    }
}
