<?php

namespace App\Http\Controllers;

use App\Models\Note;
use App\Models\NoteItem;
use App\Http\Resources\NoteItemResource;
use Illuminate\Http\Request;

class NotesItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function index(Note $note)
    {
        return NoteItemResource::collection($note->items()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Note $note)
    {
        $noteItem = NoteItem::create([
            'note_id'      => $note->id,
            'content'      => $request->content,
            'is_checked'   => $request->is_checked,
            'order_number' => $request->order_number ?? 1,
        ]);
        return new NoteItemResource($noteItem);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Note  $note
     * @param  Integer  $noteItemId
     * @return \Illuminate\Http\Response
     */
    public function show(Note $note, $noteItemId)
    {
        return new NoteItemResource(NoteItem::find($noteItemId));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Note  $note
     * @param  Integer  $noteItemId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Note $note, $noteItemId)
    {
        $noteItem = NoteItem::findOrFail($noteItemId);
        $noteItem->update($request->only([
            'content',
            'is_checked',
            'order_number'
        ]));
        return new NoteItemResource($noteItem);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Note  $note
     * @param  Integer  $noteItemId
     * @return \Illuminate\Http\Response
     */
    public function destroy(Note $note, $noteItemId)
    {
        $noteItem = NoteItem::findOrFail($noteItemId);
        $noteItem->delete();
        return response()->json(null, 204);
    }
}
