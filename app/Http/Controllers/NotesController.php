<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Note;
use App\Http\Resources\NoteResource;

class NotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return NoteResource::collection(Note::with('user')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $note = Note::create([
            'user_id'     => Auth::id() ?? 1,   // 1 - for tests (unauthorized)
            'name'        => $request->name,
            'description' => $request->description,
            'color'       => $request->color
        ]);
        return new NoteResource($note);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function show(Note $note)
    {
        return new NoteResource($note);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Note $note)
    {
        $note->update($request->only([
            'name',
            'description',
            'color'
        ]));
        return new NoteResource($note);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function destroy(Note $note)
    {
        // TODO: remove related
        $note->delete();
        return response()->json(null, 204);
    }

    /**
     * Show view note details and items, tags, comments, management.
     */
    public function details($noteId)
    {
        $note = Note::findOrFail($noteId);
        return view('notes')->with('data', new NoteResource($note));
    }
}
