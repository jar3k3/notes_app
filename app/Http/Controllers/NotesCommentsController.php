<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Note;
use App\Models\NoteComment;
use App\Http\Resources\NoteCommentResource;

class NotesCommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function index(Note $note)
    {
        return NoteCommentResource::collection($note->comments()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Note $note)
    {
        $noteComment = NoteComment::create([
            'note_id'      => $note->id,
            'user_id'      => Auth::id() ?? 1,   // 1 - for tests (unauthorized)
            'content'      => $request->content
        ]);
        return new NoteCommentResource($noteComment);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Note  $note
     * @param  Integer  $noteCommentId
     * @return \Illuminate\Http\Response
     */
    public function show(Note $note, $noteCommentId)
    {
        return new NoteCommentResource(NoteComment::find($noteCommentId));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Note  $note
     * @param  Integer  $noteCommentId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Note $note, $noteCommentId)
    {
        $noteComment = NoteComment::findOrFail($noteCommentId);
        $noteComment->update($request->only([
            'content'
        ]));
        return new NoteCommentResource($noteComment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Note  $note
     * @param  Integer  $noteItemId
     * @return \Illuminate\Http\Response
     */
    public function destroy(Note $note, $noteCommentId)
    {
        $noteComment = NoteComment::findOrFail($noteCommentId);
        $noteComment->delete();
        return response()->json(null, 204);
    }
}
