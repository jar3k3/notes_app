<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Note;
use App\Models\NoteTag;
use App\Http\Resources\NoteResource;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notesData = NoteResource::collection(Note::with('user')->get());
        $tagsNames = array_unique(NoteTag::pluck('name')->toArray());
        return view('home')->with('notes', $notesData)
                           ->with('tagsNames', $tagsNames);
    }
}
