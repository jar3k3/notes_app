<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NoteTag extends Model
{
    use SoftDeletes;

    protected $table = 'notes_tags';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'note_id',
        'name'
    ];

    public function note() {
        return $this->belongsTo('App\Models\Note', 'note_id', 'id');
    }
}
