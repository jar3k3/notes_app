<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NoteItem extends Model
{
    use SoftDeletes;

    protected $table = 'notes_items';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'note_id',
        'content',
        'is_checked',
        'order_number'
    ];
    protected $attributes = [
        'is_checked' => 0
    ];

    public function note() {
        return $this->belongsTo('App\Models\Note', 'note_id', 'id');
    }
}
