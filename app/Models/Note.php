<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Note extends Model
{
    use SoftDeletes;

    protected $table = 'notes';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'name',
        'description',
        'color'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
    public function items() {
        return $this->hasMany('App\Models\NoteItem', 'note_id', 'id');
    }
    public function tags() {
        return $this->hasMany('App\Models\NoteTag', 'note_id', 'id');
    }
    public function comments() {
        return $this->hasMany('App\Models\NoteComment', 'note_id', 'id');
    }
}
