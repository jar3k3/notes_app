<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NoteComment extends Model
{
    use SoftDeletes;

    protected $table = 'notes_comments';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'note_id',
        'user_id',
        'content'
    ];

    public function note() {
        return $this->belongsTo('App\Models\Note', 'note_id', 'id');
    }
    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
